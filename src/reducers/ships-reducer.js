import {
    GET_SHIPS_LIST_INIT,
    GET_SHIPS_LIST_SUCCESS,
    GET_SHIPS_LIST_FAILED 
 } from '../actions/Actions';

 export const initialState = {
    
    shipsList : []
};

export default function missionsReducer(state = initialState, action) {
const handlers = {
  [GET_SHIPS_LIST_INIT]: (state) => ({
    ...state,
  }),
  [GET_SHIPS_LIST_SUCCESS]: (state, action) => ({
    ...state,
    shipsList: action.response,
    
  }),
  [GET_SHIPS_LIST_FAILED]: (state) => ({
    ...state,
    shipsList: [],
  }),
  
};

    const handler = handlers[action.type];
    if (!handler) return state;
    
    return { ...state, ...handler(state, action) };
  }
  
  