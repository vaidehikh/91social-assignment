import {
    GET_MISSIONS_LIST_INIT,
    GET_MISSIONS_LIST_SUCCESS,
    GET_MISSIONS_LIST_FAILED 
 } from '../actions/Actions';

 export const initialState = {
    
    missionsList : []
};

export default function missionsReducer(state = initialState, action) {
const handlers = {
  [GET_MISSIONS_LIST_INIT]: (state) => ({
    ...state,
  }),
  [GET_MISSIONS_LIST_SUCCESS]: (state, action) => ({
    ...state,
    missionsList: action.response,
    
  }),
  [GET_MISSIONS_LIST_FAILED]: (state) => ({
    ...state,
    missionsList: [],
  }),
  
};

    const handler = handlers[action.type];
    if (!handler) return state;
    
    return { ...state, ...handler(state, action) };
  }
  
  