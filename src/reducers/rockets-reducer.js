import {
    GET_ROCKETS_LIST_INIT,
    GET_ROCKETS_LIST_SUCCESS,
    GET_ROCKETS_LIST_FAILED 
 } from '../actions/Actions';

 export const initialState = {
    
    rocketsList : []
};

export default function missionsReducer(state = initialState, action) {
const handlers = {
  [GET_ROCKETS_LIST_INIT]: (state) => ({
    ...state,
  }),
  [GET_ROCKETS_LIST_SUCCESS]: (state, action) => ({
    ...state,
    rocketsList: action.response,
    
  }),
  [GET_ROCKETS_LIST_FAILED]: (state) => ({
    ...state,
    rocketsList: [],
  }),
  
};

    const handler = handlers[action.type];
    if (!handler) return state;
    
    return { ...state, ...handler(state, action) };
  }
  
  