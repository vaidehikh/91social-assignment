import {
    GET_LOGIN_INIT,
    GET_LOGIN_SUCCESS,
    GET_LOGIN_FAILED,
  
  } from '../actions/Actions';
  
  export const initialState = {
   
  roleList: []
  };
  
  export default function loginReducer(state = initialState, action) {
    console.log(action)
    const handlers = {
      [GET_LOGIN_INIT]: (state) => ({
        ...state,
      
      }),
      [GET_LOGIN_SUCCESS]: (state, action) => ({
        ...state,
        
        roleList: action.response
  
      }),
      [GET_LOGIN_FAILED]: (state) => ({
        ...state,
      
        roleList: []
      }),
  
    };
  
  
    const handler = handlers[action.type];
    if (!handler) return state;
    
    return { ...state, ...handler(state, action) };
  }
  
  