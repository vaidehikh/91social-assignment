import React from 'react';
import logo from './logo.svg';
import './App.css';
import Login from './components/Login';
import { createBrowserHistory } from 'history';
import { BrowserRouter } from 'react-router-dom';
import Home from './components/Home';

function App() {
  return (
    <div className="App">
    
   <BrowserRouter history={createBrowserHistory}>
   
   <Home/>
   
   </BrowserRouter >
   
  </div>
  );
}

export default App;
