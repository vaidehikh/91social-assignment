import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import missions from './reducers/missions-reducer';
import rockets from './reducers/rockets-reducer';
import ships from './reducers/ships-reducer';
import role from './reducers/login-reducer'

const reducers = combineReducers({
 missions,
 rockets,
 ships,
 role
});
const initialState = {};
const enhancers = [];
const middleware = [thunk];

const { devToolsExtension } = window;
if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension());
}

const composedEnhancers = compose(applyMiddleware(...middleware), ...enhancers);
const store = createStore(reducers, initialState, composedEnhancers);

export default store;