import React, { Component } from "react";
import { withRouter, Route } from "react-router";
import { connect } from "react-redux";
import { getMissionsList } from "../actions/Actions";
import BackgroundMission from '../assets/images/satellite.jpg';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Header from "./Header";

const styles = {
    header: {
        backgroundImage: `url(${BackgroundMission})`,
        margin: '0px auto',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        
        color: 'white',
    },

    content: {
        width: '50%',
        backgroundColor: 'rgba(0, 0, 0, 0.2)',
        color: 'white',
        padding: "10px"
       
    }
}

class Missions extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount(props) {

        this.props.dispatch(getMissionsList());

    }
    render() {
        return (
            <>
                <div style={styles.header}>
                
                    <div style={{ padding:"10px", display: "flex", justifyContent: "center", alignItems: "center" }}>
                        <Header></Header>
                    </div>

                    {
                        this.props.missionsList.map((item, i) => (
                            console.log(this.props.match.params.user),
                            <div style={{ display: "flex", justifyContent: "center", alignItems: "center" }} >
                               
                                <Card style={styles.content}>
                                    <CardContent>
                                        <Typography gutterBottom>
                                            {item.flight_number} {item.mission_name}
                                        </Typography>
                                        <Typography gutterBottom>
                                            {item.details}
                                        </Typography>
                                    </CardContent>
                                </Card>
                            </div>

                        ))

                    }

                </div>
            </>
        )
    }
}

const select = (state) => ({
    missionsList: state.missions.missionsList,

});

export default withRouter(connect(select)(Missions));