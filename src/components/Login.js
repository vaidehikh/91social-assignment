import React, { Component } from "react";
import { Button, FormGroup, Input, InputLabel } from '@material-ui/core';
import '../App.css';
import { withRouter } from 'react-router-dom';
import Background from '../assets/images/space.jpg';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { connect } from "react-redux";
import { getLogin } from "../actions/Actions";

const vaildEmail = (val) => /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(val)

const styles = {
    header: {
        backgroundImage: `url(${Background})`,
        margin: '0px auto',
        height: '100vh',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
    },

    content: {
        width: '50%',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        color: 'white',
        display: "flex",
        justifyContent: "center",
        alignItems: "center",

    }
}

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            
            user: '',

        };
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.userTypeChange = this.userTypeChange.bind(this);
    }

    userTypeChange(value) {
        this.setState({
            user: value
        });

    }

    onChange(event) {
        this.setState({
            [event.target.id]: event.target.value
        });

    }



    isFormInValid() {
        let isInvalid = false;
        if (this.state.email === undefined || this.state.email === '' || this.state.user === '' || !vaildEmail(this.state.email)) {
            isInvalid = true;

        }
        if (this.state.password === undefined || this.state.password === '' || this.state.user === '') {
            isInvalid = true;

        }
        return isInvalid;
    }
    onSubmit() {
        const { email, password, user } = this.state;

        this.props.history.push(`/missions/${user}`);
    }

    componentDidMount(props) {

        this.props.dispatch(getLogin());

    }
    

    render() {
        const { email, password, user } = this.state;
        return (
           
            <>

                <div style={styles.header}>
                    <div style={styles.content}>
                        <form>
                            <FormGroup style={{ padding: '10px' }}>
                                <InputLabel style={{ color: 'white' }} htmlFor="user-type">User Type</InputLabel>
                                <Select style={{ color: 'white' }}
                                    name="name"
                                    value={user}
                                    onChange={(event) => this.userTypeChange(event.target.value)}>

                                    {this.props.roleList.role && Object.values(this.props.roleList.role).map((roleModel, index) =>
                                        <MenuItem key={index} value={roleModel}>{roleModel}</MenuItem>
                                    )}

                                    
                                </Select>
                            </FormGroup>
                            <FormGroup style={{ padding: '10px' }}>
                                <InputLabel style={{ color: 'white' }} htmlFor='email'>email</InputLabel>
                                <Input style={{ color: 'white' }} type="text" id="email" name="email"

                                    value={email}
                                    onChange={this.onChange} />
                            </FormGroup>
                            <FormGroup style={{ padding: '10px' }}>
                                <InputLabel htmlFor="password" style={{ color: 'white' }}>Password</InputLabel>
                                <Input style={{ color: 'white' }} type="password" id="password" name="password"

                                    value={password}
                                    onChange={this.onChange} />
                            </FormGroup>
                            <FormGroup style={{ padding: '10px' }}>
                                <Button color='secondary'
                                    disabled={!this.isFormInValid()}>
                                    Please enter the valid email and user type</Button>
                                <Button variant="contained" color='primary'
                                    disabled={this.isFormInValid()}
                                    onClick={this.onSubmit}>Login</Button>

                            </FormGroup>
                        </form >
                    </div>
                </div>
            </>
        )
    }
}

const select = (state) => ({
    roleList: state.role.roleList,

});
export default withRouter(connect(select)(Login));
