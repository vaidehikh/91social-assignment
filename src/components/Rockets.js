import React, { Component } from "react";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import { getRocketsList } from "../actions/Actions";
import BackgroundRocket from '../assets/images/rocket.jpg';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Header from "./Header";

const styles = {
    header: {
        backgroundImage: `url(${BackgroundRocket})`,
        margin: '0px auto',
         height: '100%',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        // display: "fixed",
        // justifyContent: "center",
        // alignItems: "center",
        color: 'white',
    },

    content: {
        width: '50%',
        backgroundColor: 'rgba(0, 0, 0, 0.2)',
        color: 'white',
        padding: "10px",
        // height: '100%',
        // display: "flex",
        // justifyContent: "center",
        // alignItems: "center",

    }
}

class Rockets extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount(props) {

        this.props.dispatch(getRocketsList());

    }
    render() {
        return (
            <>
                <div style={styles.header}>
                    <div style={{ padding:"10px", display: "flex", justifyContent: "center", alignItems: "center" }}>
                        <Header></Header>
                    </div>

                    {
                        this.props.rocketsList.map((item, i) => (
                            console.log(item.flight_number),
                            <div style={{ display: "flex", justifyContent: "center", alignItems: "center" }} >
                                <Card style={styles.content}>
                                    <CardContent>
                                        <Typography gutterBottom>
                                            {item.first_flight}
                                        </Typography>
                                        <Typography gutterBottom>
                                            {item.country}
                                        </Typography>
                                        <Typography gutterBottom>
                                            {item.company}
                                        </Typography>
                                    </CardContent>
                                </Card>
                            </div>

                        ))

                    }

                </div>
            </>
        )
    }
}

const select = (state) => ({
    rocketsList: state.rockets.rocketsList,

});

export default withRouter(connect(select)(Rockets));