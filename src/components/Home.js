import React, { Component } from "react";
import { Switch, Route, withRouter } from 'react-router-dom';
import Login from "./Login";
import Missions from "./Missions";
import Rockets from "./Rockets";
import Ships from "./Ships";
import About from "./About";

class Home extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <Switch>
                    <Route path='/' exact component={Login} />
                    <Route path='/missions/:user' exact component={() => <Missions />} />
                    <Route path='/rockets/:user' exact component={() => <Rockets />} />
                    <Route path='/ships/:user' exact component={() => <Ships />} />
                    <Route path='/about/:user' exact component={() => <About />} />
                </Switch>
            </div>
        )
    }
}

export default withRouter(Home);