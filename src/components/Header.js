import React, { Component } from "react";
import { withRouter } from 'react-router-dom';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';
class Header extends Component {
    constructor(props) {
        super(props);
      
    }
    
    render() {
       
        return (
           
            <div>
                <h1>Welcome {this.props.match.params.user}</h1>
               <Breadcrumbs>
               <Link style={{ color: 'white' }} href={'/missions/'+this.props.match.params.user}>Missions</Link>
                      <Link style={{ color: 'white' }} href={'/rockets/'+this.props.match.params.user}>Rockets</Link>
                      <Link style={{ color: 'white' }} href = {'/ships/'+this.props.match.params.user}>Ships</Link>
                      <Link style={{ color: 'white' }} href = {'/about/'+this.props.match.params.user}>About</Link>
              </Breadcrumbs >

            </div>
        )
    }
}

export default withRouter(Header);