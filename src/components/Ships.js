import React, { Component } from "react";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import { getShipsList } from "../actions/Actions";
import BackgroundShip from '../assets/images/ship.jpg';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Header from "./Header";
import CardMedia from '@material-ui/core/CardMedia';

const styles = {
    header: {
        backgroundImage: `url(${BackgroundShip})`,
        margin: '0px auto',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        
        color: 'white',
    },

    content: {
        width: '50%',
        backgroundColor: 'rgba(0, 0, 0, 0.2)',
        color: 'white',
        padding: "10px",
       

    }
}

class Ships extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount(props) {

        this.props.dispatch(getShipsList());

    }
    render() {
        return (
            <>
                <div style={styles.header}>
                    <div style={{ padding:"10px", display: "flex", justifyContent: "center", alignItems: "center" }}>
                        <Header></Header>
                    </div>
                    
                    {
                        this.props.shipsList.map((item, i) => (
                             
                            <div style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
                                <Card style={styles.content}>
                                    <CardContent>
                                        <Typography gutterBottom>
                                            {item.ship_name}
                                        </Typography>
                                        {/* <Typography gutterBottom>
                                            {item.country}
                                        </Typography> */}
                                        <CardMedia
                                        style={{height: 0, paddingTop: '50%'}}
                                            image={item.image}
                                            //title="Paella dish"
                                        />
                                        <Typography gutterBottom>
                                            {item.home_port}
                                        </Typography>
                                    </CardContent>
                                </Card>
                            </div>    
                                
                               
                        ))

                    }
                   
                </div>
            </>
        )
    }
}

const select = (state) => ({
    shipsList: state.ships.shipsList,

});

export default withRouter(connect(select)(Ships));