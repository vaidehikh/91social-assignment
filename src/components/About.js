import React, { Component } from "react";
import { withRouter } from "react-router";
import Typography from '@material-ui/core/Typography';
import Background from '../assets/images/space.jpg';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Header from "./Header";

const styles = {
    header: {
        backgroundImage: `url(${Background})`,
        margin: '0px auto',
        height: '100%',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',

    },

    content: {
        width: '60%',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        color: 'white',
        height: '100%',

    }
}


class About extends Component {
    render() {
        return (
            <div style={styles.header}>
                <div style={{ padding: "10px", display: "flex", justifyContent: "center", alignItems: "center" }}>
                    <Header></Header>
                </div>
                <div style={{ padding: "50px", display: "flex", justifyContent: "center", alignItems: "center" }}>
                    <Card style={styles.content}>
                        <CardContent>
                            <Typography variant="h5">About</Typography>
                            <br />
                            <Typography paragraph>
                                <p>
                                    It's always excited and amazed when think of being able to flying to
                                    Mars in this lifetime.
                                </p>
                                <p>
                                    And of course, appreciate the hard work for all of those who devoted
                                    their careers to human space development{" "}
                                    <span role="img" aria-label="rocket">
                                        🚀🚀🚀
                                </span>
                                </p>

                                <p>
                                    And of course, appreciate the hard work for all of those who devoted
                                    their careers to human space development{" "}
                                    <span role="img" aria-label="rocket">
                                        🚀🚀🚀
                                    </span>
                                </p>
                                <p>
                                    The website leverages{" "}
                                    <a href="https://spacex.com" text="SpaceX API" /> and{" "}
                                    <a href="https://spacex.com" text="NASA API" /> to display SpaceX's
                                        mission information and the weather on Mars. It's written using
                                        ReactJS. This site is not for commercial use, and doesn't
                                        have relations to either SpaceX or NASA in any kinds.
                                        </p>
                                        This website was made for educational and fan purposes, and has no affiliation with&nbsp;
                                        <a href="https://spacex.com">
                                    SpaceX.
                                        </a>
                                        &nbsp;
                                        </Typography>
                            <Typography paragraph>
                                The API used to gather information on this website is made by Jake Meyer, and can be found&nbsp;
                                        <a href="https://github.com/r-spacex/SpaceX-API">
                                    here.
                                        </a>
                            </Typography>

                        </CardContent>
                    </Card>
                </div>
            </div>
        );
    }
}
export default withRouter(About)