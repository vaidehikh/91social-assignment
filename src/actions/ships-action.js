import { 
    getShipsListApi
  } from '../api/ships-api';

export const GET_SHIPS_LIST_INIT = 'GET_SHIPS_LIST_INIT';
export const GET_SHIPS_LIST_SUCCESS = 'GET_SHIPS_LIST_SUCCESS';
export const GET_SHIPS_LIST_FAILED = 'GET_SHIPS_LIST_FAILED';

export function getShipsList() {
    return function (dispatch) {
      dispatch({ type: GET_SHIPS_LIST_INIT });
      
      getShipsListApi()
      
      .then((response) => {
      
            return dispatch({ type: GET_SHIPS_LIST_SUCCESS, response })
         
        })
        
        .catch(error => dispatch({ type: GET_SHIPS_LIST_FAILED, error }));
    };
}

