import {
    loginAPI
} from '../api/login-api';

export const GET_LOGIN_INIT = 'GET_LOGIN_INIT';
export const GET_LOGIN_SUCCESS = 'GET_LOGIN_SUCCESS';
export const GET_LOGIN_FAILED = 'GET_LOGIN_FAILED';

export function getLogin() {

    return function (dispatch) {
        dispatch({ type: GET_LOGIN_INIT });
        loginAPI()
            .then((response) => {

                return dispatch({ type: GET_LOGIN_SUCCESS, response })

            })

            .catch((error) => dispatch({ type: GET_LOGIN_FAILED, error }));
    };
}


