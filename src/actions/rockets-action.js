import { 
    getRocketsListApi
  } from '../api/rockets-api';

export const GET_ROCKETS_LIST_INIT = 'GET_ROCKETS_LIST_INIT';
export const GET_ROCKETS_LIST_SUCCESS = 'GET_ROCKETS_LIST_SUCCESS';
export const GET_ROCKETS_LIST_FAILED = 'GET_ROCKETS_LIST_FAILED';

export function getRocketsList() {
    return function (dispatch) {
      dispatch({ type: GET_ROCKETS_LIST_INIT });
      
      getRocketsListApi()
      
      .then((response) => {
      
            return dispatch({ type: GET_ROCKETS_LIST_SUCCESS, response })
         
        })
        
        .catch(error => dispatch({ type: GET_ROCKETS_LIST_FAILED, error }));
    };
}

