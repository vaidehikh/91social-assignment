import { 
    getMissionsListApi
  } from '../api/missions-api';

export const GET_MISSIONS_LIST_INIT = 'GET_MISSIONS_LIST_INIT';
export const GET_MISSIONS_LIST_SUCCESS = 'GET_MISSIONS_LIST_SUCCESS';
export const GET_MISSIONS_LIST_FAILED = 'GET_MISSIONS_LIST_FAILED';

export function getMissionsList() {
    return function (dispatch) {
      dispatch({ type: GET_MISSIONS_LIST_INIT });
      
      getMissionsListApi()
      
      .then((response) => {
      
            return dispatch({ type: GET_MISSIONS_LIST_SUCCESS, response })
         
        })
        
        .catch(error => dispatch({ type: GET_MISSIONS_LIST_FAILED, error }));
    };
}

