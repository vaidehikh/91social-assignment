export function loginAPI() {

    var requestOptions = {
        method: 'GET',
        redirect: 'follow'
    };


    return fetch(`https://run.mocky.io/v3/737b3d5b-0b33-4ca9-9f44-2aa80b87743b`, requestOptions)

        .then((res) => {

            if (res.status === 401) {
                return 'invalid authentication';
            } else if (res.status !== 200) {
                return 'failed';
            }
            return res.json();
        }).catch(err => 'failed');
}