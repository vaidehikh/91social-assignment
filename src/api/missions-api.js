export function getMissionsListApi() {

    console.log("getMissionsListApi method call")
        ;
    var requestOptions = {
        method: 'GET',
        redirect: 'follow',

    };

    return fetch(`https://api.spacexdata.com/v3/launches/past`, requestOptions)

        .then((res) => {
            if (res.status === 401) {
                return 'invalid authentication';
            } else if (res.status !== 200) {
                return 'failed';
            }
            return res.json();
        })
        
        .catch(err => err);
}